package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	"github.com/alecthomas/gometalinter/_linters/src/gopkg.in/yaml.v2"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/vcd-exporter/measure"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
)

const (
	defaultLabelsFilter = "^vcd"
)

type VcdConfigs struct {
	VcdConfigurations []VcdConfiguration `yaml:"vcd_endpoints"`
}

type VcdConfiguration struct {
	VcdHost     string   `yaml:"vcd_host"`
	VcdOrg      string   `yaml:"vcd_org"`
	VcdUsername string   `yaml:"vcd_user"`
	VcdPassword string   `yaml:"vcd_password"`
	IgnoreSSL   bool     `yaml:"ignore_ssl"`
	VmData      bool     `yaml:"vm_data"`
	NetData     bool     `yaml:"net_data"`
	StorageData bool     `yaml:"storage_data"`
}

var (
	namespace = getEnv("NAMESPACE", "vcd")
	log           = logrus.New()
	logLevel      = getEnv("LOG_LEVEL", "info")
	labelsFilter  = getEnv("LABELS_FILTER", defaultLabelsFilter)
	metricsPath   = getEnv("METRICS_PATH", "/metrics")
	listenAddress = getEnv("LISTEN_ADDRESS", ":9173")
	configPath    = getEnv("CONFIG_PATH", "/opt/vcd_config/vcd_config.yml")
)

func getEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func main() {
	/*
	 TODO: Log some of the configuration details
	 TODO: Add handler for ?target= methods for metric collection
	*/
	flag.Parse()

	setLogLevel(logLevel)

	log.Info("Starting vCD Promtheus Exporter")

	labelsFilterRegexp, err := regexp.Compile(labelsFilter)

	if err != nil {
		log.Fatal(err)
	}

	// Metrics about exporter functions
	measure.Init()

	// Load configuration file
	filename, _ := filepath.Abs(configPath)
	configFile, err := ioutil.ReadFile(filename)

	if err != nil {
		log.Fatal(err)
	}

	var configs VcdConfigs

	err = yaml.Unmarshal(configFile, &configs)

	if err != nil {
		log.Fatal(err)
	}

	// Load configuration into exporterList
	exporterList := NewExporterList(labelsFilterRegexp, configs)

	for _, exporter := range exporterList.Exporters {
		prometheus.MustRegister(exporter)
	}

	http.Handle(metricsPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err = w.Write([]byte(`<html>
							<head><title>vCD Exporter</title></head>
							<body>
								<h1>vcd exporter</h1>
								<p><a href='` + metricsPath + `'>Metrics</a></p>
							<body>
						<html>
			`))
		if err != nil {
			log.Fatal(err)
		}
	})

	log.Printf("Starting Server on port %s and metrics at %s", listenAddress, metricsPath)
	log.Fatal(http.ListenAndServe(listenAddress, nil))
}
