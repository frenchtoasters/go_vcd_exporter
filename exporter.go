package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"regexp"
	"sync"
)

type Exporter struct {
	labelsFilter *regexp.Regexp
	config       *VcdConfiguration
	mutex        sync.RWMutex
	gaugeVecs    map[string]*prometheus.GaugeVec
}

type ExporterList struct {
	Exporters []*Exporter
}

func newExporter(labelsFilter *regexp.Regexp, config *VcdConfiguration) *Exporter {
	gaugeVecs := addMetrics()
	return &Exporter{
		labelsFilter: labelsFilter,
		gaugeVecs:    gaugeVecs,
		config:       config,
	}
}
func (list *ExporterList) addExporter(exporter *Exporter) []*Exporter {
	list.Exporters = append(list.Exporters, exporter)
	return list.Exporters
}

//TODO: make the labelsFilter a list
func NewExporterList(labelsFilter *regexp.Regexp, configs VcdConfigs) ExporterList {
	var list ExporterList
	for _, config := range configs.VcdConfigurations {
		e := newExporter(labelsFilter, &config)
		list.addExporter(e)
	}
	return list
}
