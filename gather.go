package main

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/vmware/go-vcloud-director/v2/govcd"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"net/http"
	"net/url"
	"strconv"
)

type Data struct {
	Orgs []*vOrg
}

type StorageProfile struct {
	vdcId string
	name string
	id string
	provisionedMB int64
	requestedMB int64
	totalMB int64
	usedMB int64
	reference *types.Reference
}

type vapp struct {
	vdcId string
	vappId string
	vappClient govcd.VApp
	vms []*vm
}

type vm struct {
	vdcId string
	vappId string
	metaData map[string]string
	networkConnectionSection *types.NetworkConnectionSection
	virtualHardwareSection *types.VirtualHardwareSection
	storageProfile *types.Reference
	vmClient govcd.VM
}

type orgVdcNetwork struct {
	vdcId string
	vdcnetwork []govcd.OrgVDCNetwork
}

type edgeGateway struct {
	vdcId string
	edgeGateways []govcd.EdgeGateway
}

type vOrg struct {
	id string
	name string
	org govcd.Org
	vdcs []govcd.Vdc
	vapps []*vapp
	vdcnetworks orgVdcNetwork
	vdcstorageprofiles []*StorageProfile
	edgegateways edgeGateway
}

func OrgList(vcdClient *govcd.VCDClient) (*types.OrgList, error) {
	// TODO: move this into a function that is part of the Data type, requires changes else where
	orgListHREF := vcdClient.Client.VCDHREF
	orgListHREF.Path += "/org"

	orgList := new(types.OrgList)

	_, err := vcdClient.Client.ExecuteRequest(orgListHREF.String(), http.MethodGet,
		"", "Error Retrieving orgList: %s", nil, orgList)

	if err != nil {
		return orgList, err
	}
	return orgList, nil
}

func (d *Data) addData(org *vOrg) *Data {
	d.Orgs = append(d.Orgs, org)
	return d
}

func (v *vOrg) findStorageProfileReferences(vcdClient *govcd.VCDClient, vdcProfiles []*types.VdcStorageProfiles, vdcId string) *vOrg {
	for _, profileReferences := range vdcProfiles {
		for _, profileReference := range profileReferences.VdcStorageProfile {
			profile := new(StorageProfile)
			profile.name = profileReference.Name
			profile.id = profileReference.ID
			profile.reference = profileReference
			profile.vdcId = vdcId
			results, err := govcd.QueryProviderVdcStorageProfileByName(vcdClient, profile.name)
			if err != nil {
				log.Errorf("Unable to gather storage profile information: %s\n", err)
			}
			for _, storage := range results {
				profile.provisionedMB = storage.StorageProvisionedMB
				profile.requestedMB = storage.StorageRequestedMB
				profile.totalMB = storage.StorageTotalMB
				profile.usedMB = storage.StorageUsedMB
			}
			v.vdcstorageprofiles = append(v.vdcstorageprofiles, profile)
		}
	}

	return v
}

func (v *vOrg) addStorageProfile(profileReference *types.Reference) *vOrg {
	profile := new(StorageProfile)
	profile.name = profileReference.Name
	profile.id = profileReference.ID
	profile.reference = profileReference
	v.vdcstorageprofiles = append(v.vdcstorageprofiles, profile)
	return v
}

func (v *vOrg) addVdcNetworkList(vdc govcd.Vdc) *vOrg {
	err := vdc.Refresh()
	if err != nil {
		return v
	}

	for _, an := range vdc.Vdc.AvailableNetworks {
		for _, reference := range an.Network {
			orgNet, err := vdc.FindVDCNetwork(reference.Name)
			if err != nil {
				return v
			}
			v.vdcnetworks.vdcId = vdc.Vdc.ID
			if orgNet.OrgVDCNetwork.Configuration == nil {
				orgNet.OrgVDCNetwork.Configuration = &types.NetworkConfiguration{}
				orgNet.OrgVDCNetwork.Configuration.Features.DhcpService = &types.DhcpService{}
				orgNet.OrgVDCNetwork.Configuration.Features.DhcpService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService = &types.StaticRoutingService{}
				orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.LoadBalancerService = &types.LoadBalancerService{}
				orgNet.OrgVDCNetwork.Configuration.Features.LoadBalancerService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.NatService = &types.NatService{}
				orgNet.OrgVDCNetwork.Configuration.Features.NatService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.FirewallService = &types.FirewallService{}
				orgNet.OrgVDCNetwork.Configuration.Features.FirewallService.IsEnabled = false
			} else if orgNet.OrgVDCNetwork.Configuration.Features == nil  {
				orgNet.OrgVDCNetwork.Configuration.Features = &types.NetworkFeatures{}
				orgNet.OrgVDCNetwork.Configuration.Features.DhcpService = &types.DhcpService{}
				orgNet.OrgVDCNetwork.Configuration.Features.DhcpService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService = &types.StaticRoutingService{}
				orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.LoadBalancerService = &types.LoadBalancerService{}
				orgNet.OrgVDCNetwork.Configuration.Features.LoadBalancerService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.NatService = &types.NatService{}
				orgNet.OrgVDCNetwork.Configuration.Features.NatService.IsEnabled = false
				orgNet.OrgVDCNetwork.Configuration.Features.FirewallService = &types.FirewallService{}
				orgNet.OrgVDCNetwork.Configuration.Features.FirewallService.IsEnabled = false
			} else {
				if orgNet.OrgVDCNetwork.Configuration.Features.FirewallService == nil {
					orgNet.OrgVDCNetwork.Configuration.Features.FirewallService = &types.FirewallService{}
					orgNet.OrgVDCNetwork.Configuration.Features.FirewallService.IsEnabled = false
				}
				if orgNet.OrgVDCNetwork.Configuration.Features.LoadBalancerService == nil {
					orgNet.OrgVDCNetwork.Configuration.Features.LoadBalancerService = &types.LoadBalancerService{}
					orgNet.OrgVDCNetwork.Configuration.Features.LoadBalancerService.IsEnabled = false
				}
				if orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService == nil {
					orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService = &types.StaticRoutingService{}
					orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService.IsEnabled = false
				}
				if orgNet.OrgVDCNetwork.Configuration.Features.NatService == nil {
					orgNet.OrgVDCNetwork.Configuration.Features.NatService = &types.NatService{}
					orgNet.OrgVDCNetwork.Configuration.Features.NatService.IsEnabled = false
				}
				if orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService == nil {
					orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService = &types.StaticRoutingService{}
					orgNet.OrgVDCNetwork.Configuration.Features.StaticRoutingService.IsEnabled = false
				}
				if orgNet.OrgVDCNetwork.Configuration.Features.DhcpService == nil {
					orgNet.OrgVDCNetwork.Configuration.Features.DhcpService = &types.DhcpService{}
					orgNet.OrgVDCNetwork.Configuration.Features.DhcpService.IsEnabled = false
				}
			}
			v.vdcnetworks.vdcnetwork = append(v.vdcnetworks.vdcnetwork, orgNet)
		}
	}
	return v
}

func (v *vOrg) addVdcEdgeGatewayList(vcdClient *govcd.VCDClient, vdc govcd.Vdc) (*vOrg, error) {
	err := vdc.Refresh()
	if err != nil {
		return v, err
	}

	for _, av := range vdc.Vdc.Link {
		if av.Rel == "edgeGateways" && av.Type == types.MimeQueryRecords {
			query := new(types.QueryResultEdgeGatewayRecordsType)

			_, err := vcdClient.Client.ExecuteRequest(av.HREF, http.MethodGet,
				"", "error quering edge gateways: %s", nil, query)
			if err != nil {
				return v, err
			}
			for _, edge := range query.EdgeGatewayRecord {
				e, err := vdc.FindEdgeGateway(edge.Name)
				if err != nil {
					return v, err
				}
				v.edgegateways.edgeGateways = append(v.edgegateways.edgeGateways, e)
			}
		}
	}
	return v, nil
}

func (e *Exporter) Client() (*govcd.VCDClient, error) {
	u, err := url.ParseRequestURI(fmt.Sprintf("https://%s/api", e.config.VcdHost))

	if err != nil {
		log.Fatal(err)
	}

	vcdclient := govcd.NewVCDClient(*u, e.config.IgnoreSSL)
	err = vcdclient.Authenticate(e.config.VcdUsername, e.config.VcdPassword, e.config.VcdOrg)

	if err != nil {
		log.Fatal(err)
	}
	return vcdclient, nil
}

func (e *Exporter) processMetrics(data *Data, ch chan<- prometheus.Metric, exporter *Exporter) error {
	for _, org := range data.Orgs {
		if checkMetric(org) == false {
			log.Info("Not a real org")
			continue
		}

		log.Debugf("Processing metrics for %s", org.name)

		if err := e.setOrgMetrics(org); err != nil {
			log.Errorf("Error processing Org: %s \n", err)
		}

		if len(org.vdcs) >= 1 {
			// Set metrics for vdcs
			if err := e.setVdcMetrics(org); err != nil {
				log.Errorf("Error processing Vdc: %s \n", err)
			}
		}

		if len(org.vapps) >= 1 {
			// Set metrics for vapps
			if err := e.setVappMetrics(org); err != nil {
				log.Errorf("Error processing Vapp: %s \n", err)
			}
			if err := e.setVmMetrics(org); err != nil {
				log.Errorf("Error processing Vm: %s \n", err)
			}
		}

		if len(org.vdcnetworks.vdcnetwork) >= 1 {
			// Set metrics for vdc networks
			if err := e.setVdcNetworkMetrics(org); err != nil {
				log.Errorf("Error processing Vdc Network: %s \n", err)
			}
		}

		if len(org.vdcstorageprofiles) >= 1 {
			// Set metrics for vdc storage profiles
			if err := e.setVdcStorageProfileMetrics(org); err != nil {
				log.Errorf("Error processing Storage Profile: %s \n", err)
			}
		}

		if len(org.edgegateways.edgeGateways) >= 1 {
			// Set metrics for edge gateways
			if err := e.setEdgeGatewayMetrics(org); err != nil {
				log.Errorf("Error processing Edge Gateway: %s \n", err)
			}
		}
	}
	return nil
}

func (e *Exporter) gatherMetrics(exporter *Exporter) (*Data, error) {
	data := new(Data)
	client, err := exporter.Client()

	if err != nil {
		return data, err
	}

	log.Print("Gather Org Metrics")
	orgResult := e.gatherOrgData(data, client)
	if orgResult != nil {
		return data, orgResult
	}

	log.Print("Gather vApp Metrics")
	if e.config.VmData == true {
		vappResult := e.gatherVappData(data, client, true)
		if vappResult != nil {
			return data, vappResult
		}
	} else {
		vappResult := e.gatherVappData(data, client, false)
		if vappResult != nil {
			return data, vappResult
		}
	}

	if e.config.NetData == true {
		netResult := e.gatherNetwork(data, client)
		if netResult != nil {
			return data, netResult
		}
	}

	if e.config.StorageData == true {
		storageResult := e.gatherStorageData(data, client)
		if storageResult != nil {
			return data, storageResult
		}
	}

	return data, nil
}

func (e *Exporter) gatherOrgData(data *Data, vcdClient *govcd.VCDClient) error {
	// Get the list of Orgs that are visiable to user
	orgList, err := OrgList(vcdClient)
	if err != nil {
		return err
	}

	// Loop over the the list of Orgs
	for _, org := range orgList.Org {
		vorgData := new(vOrg)
		vorgData.org, err = govcd.GetOrgByName(vcdClient, org.Name)
		if err != nil {
			return err
		}
		vorgData.name = vorgData.org.Org.FullName
		vorgData.id = vorgData.org.Org.ID

		for _, link := range vorgData.org.Org.Link {
			if link.Type == types.MimeVDC {
				Vdc, err := vorgData.org.GetVdcByName(link.Name)
				if err != nil {
					return err
				}

				if Vdc.Vdc.Status != "1" {
					i, err := strconv.Atoi(Vdc.Vdc.Status)
					if err != nil {
						return err
					}
					log.Infof("vDC not in Ready Status, polling skipped. Status: %s\n", types.VDCStatuses[i])
				}

				vorgData.vdcs = append(vorgData.vdcs, Vdc)
				// Add found data about Org
				data.addData(vorgData)
			}
		}
	}
	return nil
}

func (e *Exporter) gatherVappData(data *Data, vcdClient *govcd.VCDClient, detail bool) error {
	// TODO: Figure out cleaner way to loop through to find this information, if possible...
	for _, orgs := range data.Orgs {
		for _, vdc := range orgs.vdcs {
			for _, resents := range vdc.Vdc.ResourceEntities {
				for _, resent := range resents.ResourceEntity {
					if resent.Type == types.MimeVApp {

						vappClient, err := vdc.FindVAppByName(resent.Name)
						if err != nil {
							log.Warn(err)
							continue
						} else {
							newVapp := new(vapp)
							newVapp.vdcId = vdc.Vdc.ID
							newVapp.vappId = vappClient.VApp.ID
							newVapp.vappClient = vappClient
							if detail {
								if vappClient.VApp.Children != nil {
									log.Info("Gathering VM information")
									for _, vmVapp := range vappClient.VApp.Children.VM {
										newVM := new(vm)
										newVM.vappId = vappClient.VApp.ID
										newVM.vdcId = vdc.Vdc.ID
										vmClient, err := vcdClient.Client.FindVMByHREF(vmVapp.HREF)

										networks, err := vmClient.GetNetworkConnectionSection()
										if err != nil {
											return err
										}
										newVM.networkConnectionSection = networks

										metaData, err := vmClient.GetMetadata()
										if err != nil {
											return err
										}

										// Lists the key, type, and value of the meta data
										if len(metaData.MetadataEntry) > 0 {
											metaMap := make(map[string]string)
											for _, meta := range metaData.MetadataEntry {
												metaMap[meta.Key] = meta.TypedValue.Value + ":" + meta.TypedValue.XsiType
											}

											newVM.metaData = metaMap
										} else {
											newVM.metaData = make(map[string]string)
										}

										if vmClient.VM.HREF == "" {
											return fmt.Errorf("vm href is empty: %s", vmClient.VM.ID)
										}

										result, err := vcdClient.Client.ExecuteRequest(vmClient.VM.HREF +
											"/virtualHardwareSection/", http.MethodGet, "",
											"error retrieving virtual hardware: %s",
											nil, newVM.virtualHardwareSection)
										if err != nil {
											return err
										} else {
											log.Debugf("Gather VirtualHardwareSection",result)
										}

										newVM.storageProfile = vmClient.VM.StorageProfile

										newVM.vmClient = vmClient
										newVapp.vms = append(newVapp.vms, newVM)
									}
								} else {
									return fmt.Errorf("No VMs in vApp: %s \n", vappClient.VApp.Name)
								}
								orgs.vapps = append(orgs.vapps, newVapp)
							}
						}
					}
				}
			}
		}
	}
	return nil
}

func (e *Exporter) gatherStorageData(data *Data, vcdClient *govcd.VCDClient) error {
	for _, org := range data.Orgs {
		for _, vdc := range org.vdcs {
			org.findStorageProfileReferences(vcdClient, vdc.Vdc.VdcStorageProfiles, vdc.Vdc.ID)
		}
	}

	return nil
}

func (e *Exporter) gatherNetwork(data *Data, vcdClient *govcd.VCDClient) error {
	for i, org := range data.Orgs {
		for _, vdc := range org.vdcs {

			data.Orgs[i].addVdcNetworkList(vdc)

			_, err := data.Orgs[i].addVdcEdgeGatewayList(vcdClient, vdc)
			if err != nil {
				return fmt.Errorf("Error Gathering vDC Edge Gateway: %s\n", err)
			}
		}
	}
	return nil
}