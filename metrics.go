package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"strconv"
)

func checkMetric(org *vOrg) bool {
	// TODO: add more checks here to fully validate object
	if org.id == "" {
		log.Info("False??")
		return false
	}
	if org.name == ""{
		log.Info("False...")
		return false
	}
	return true
}

func flattenMaps(maps []map[string]string) string{
	var flat string

	for _, m := range maps{
		if flat != "" && m["network_state"] != "" {
			flat = flat + "," + m["network_name"] + ":" + m["network_state"]
		} else {
			flat = m["network_name"] + ":" + m["network_state"]
		}

	}

	return flat
}

func addMetrics() map[string]*prometheus.GaugeVec {
	gaugeVecs := make(map[string]*prometheus.GaugeVec)

	// Defined Metrics
	gaugeVecs["orgStatus"] = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "org_health_status",
			Help:      "HealthState of vOrg reported by vCD API",
		}, []string{"org_name", "org_id", "org_status"})
	gaugeVecs["vdcStatus"] = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "vdc_health_status",
			Help:      "Status of defined vDC as reported by vCD API",
		}, []string{"vdc_name", "vdc_id", "vdc_status", "org_id", "vdc_is_enabled", "vdc_allocation_model",
			"vdc_used_network_count", "vdc_network_quota", "vdc_nic_quota", "vdc_cpu_allocated", "vdc_cpu_limit",
			"vdc_cpu_used", "vdc_cpu_overhead", "vdc_cpu_reserved", "vdc_cpu_units", "vdc_mem_allocated",
			"vdc_mem_limit", "vdc_mem_used", "vdc_mem_overhead", "vdc_mem_reserved", "vdc_mem_units"})

	// Refinded Metrics
	gaugeVecs["vappStatus"] = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "vapp_health_status",
			Help:      "Status of defined vApp as reported by vCD API",
		}, []string{"vapp_name", "vapp_id", "vapp_status", "vapp_network_names", "vapp_maintenance_status", "vdc_id", "org_id"})
	gaugeVecs["vmStatus"] = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "vm_health_status",
			Help:      "HealthState of the VM, as reported by the vCD API. Either (1) or (0)",
		}, []string{"vm_name", "vm_id", "vm_status", "vm_storage_profile_name", "vm_network_name_state",
			"vapp_id", "vdc_id", "org_id"})
	gaugeVecs["vdcNetworkStatus"] = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "vdc_network_health_status",
			Help:      "State of the vDC Network, as reported by the vCD API",
		}, []string{"network_name", "network_id", "network_status", "network_edge_gateway_id",
			"network_parent_network_id", "network_parent_network_name","network_feature_static_routing_status",
			"network_feature_nat_status", "network_feature_load_balancer_status", "network_feature_firewall_status",
			"network_feature_dhcp_status", "vdc_id", "org_id"})
	gaugeVecs["vdcStorageProfileStatus"] = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "vdc_storage_profile_health_status",
			Help:      "State of the vDC Network, as reported by the vCD API",
		}, []string{"storage_profile_name", "storage_profile_id", "storage_profile_provisioned_mb",
			"storage_profile_requested_mb", "storage_profile_total_mb", "storage_profile_used_mb", "vdc_id", "org_id"})
	gaugeVecs["edgeGatewayStatus"] = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "vdc_edge_gateway_health_status",
			Help:      "State of the vDC Edge Gateways, as reported by the vCD API",
		}, []string{"edge_gateway_name", "edge_gateway_id", "edge_gateway_status",
			"edge_gateway_firewall_service_status", "edge_gateway_dhcp_service_status",
			"edge_gateway_ipsec_service_status", "edge_gateway_load_balancer_service_status",
			"edge_gateway_nat_service_status", "edge_gateway_static_route_service_status", "vdc_id", "org_id"})

	return gaugeVecs
}

func (e *Exporter) setOrgMetrics(data *vOrg) error {
	gauge := e.gaugeVecs["orgStatus"].With(prometheus.Labels{
		"org_name": data.name,
		"org_status": strconv.FormatBool(data.org.Org.IsEnabled),
		"org_id": data.id,
	})
	if strconv.FormatBool(data.org.Org.IsEnabled) == "true" {
		gauge.Set(1)
	} else {
		gauge.Set(0)
	}
	return nil
}

func (e *Exporter) setVdcMetrics(data *vOrg) error {
	for _, vdc := range data.vdcs {
		i, err := strconv.Atoi(vdc.Vdc.Status)
		if err != nil {
			return err
		}

		computeLabels := make(map[string]string)
		for _, compute := range vdc.Vdc.ComputeCapacity {
			computeLabels["computeCpuAllocated"] = strconv.FormatInt(compute.CPU.Allocated, 10)
			computeLabels["computeCpuLimit"] = strconv.FormatInt(compute.CPU.Limit, 10)
			computeLabels["computeCpuUsed"] = strconv.FormatInt(compute.CPU.Used, 10)
			computeLabels["computeCpuOverhead"] = strconv.FormatInt(compute.CPU.Overhead, 10)
			computeLabels["computeCpuReserved"] = strconv.FormatInt(compute.CPU.Reserved, 10)
			computeLabels["computeCpuUnits"] = compute.CPU.Units // TODO: remove later
			computeLabels["computeMemAllocated"] = strconv.FormatInt(compute.Memory.Allocated, 10)
			computeLabels["computeMemLimit"] = strconv.FormatInt(compute.Memory.Limit, 10)
			computeLabels["computeMemUsed"] = strconv.FormatInt(compute.Memory.Used, 10)
			computeLabels["computeMemOverhead"] = strconv.FormatInt(compute.Memory.Overhead, 10)
			computeLabels["computeMemReserved"] = strconv.FormatInt(compute.Memory.Reserved, 10)
			computeLabels["computeMemUnits"] = compute.Memory.Units // TODO: remove later
		}


 		gauge := e.gaugeVecs["vdcStatus"].With(prometheus.Labels{
			"vdc_name": vdc.Vdc.Name,
			"vdc_id": vdc.Vdc.ID,
			"vdc_status": types.VDCStatuses[i],
			"vdc_is_enabled": strconv.FormatBool(vdc.Vdc.IsEnabled),
			"vdc_allocation_model": vdc.Vdc.AllocationModel,
			"vdc_used_network_count": strconv.Itoa(vdc.Vdc.UsedNetworkCount),
			"vdc_network_quota": strconv.Itoa(vdc.Vdc.NetworkQuota),
			"vdc_nic_quota": strconv.Itoa(vdc.Vdc.NicQuota),
			"vdc_cpu_allocated": computeLabels["computeCpuAllocated"],
			"vdc_cpu_limit": computeLabels["computeCpuLimit"],
			"vdc_cpu_used": computeLabels["comptueCpuUsed"],
			"vdc_cpu_overhead": computeLabels["computeCpuOverhead"],
			"vdc_cpu_reserved": computeLabels["computeCpuReserved"],
			"vdc_cpu_units": computeLabels["computeCpuUnits"], // TODO: remove later
			"vdc_mem_allocated": computeLabels["computeMemAllocated"],
			"vdc_mem_limit": computeLabels["computeMemLimit"],
			"vdc_mem_used": computeLabels["computeMemUsed"],
			"vdc_mem_overhead": computeLabels["computeMemOverhead"],
			"vdc_mem_reserved": computeLabels["computeMemReserved"],
			"vdc_mem_units": computeLabels["computeMemUnits"], // TODO: remove later
			"org_id": data.id,
		})
		if i == 1 {
			gauge.Set(1)
		} else {
			gauge.Set(float64(i))
		}
	}
	return nil
}

func (e *Exporter) setVappMetrics(data *vOrg) error {
	for _, vapp := range data.vapps {
		// TODO: gather metadata about vapp
		var networkLabels string
		for _, network := range vapp.vappClient.VApp.NetworkConfigSection.NetworkConfig {
			if networkLabels == "" {
				networkLabels = network.NetworkName
			} else {
				networkLabels = networkLabels + "," + network.NetworkName
			}
		}
		// TODO: update to include product section properties

		gauge := e.gaugeVecs["vappStatus"].With(prometheus.Labels{
			"vapp_name": vapp.vappClient.VApp.Name,
			"vapp_id": vapp.vappId,
			"vapp_status": types.VAppStatuses[vapp.vappClient.VApp.Status],
			"vapp_network_names": networkLabels,
			"vapp_maintenance_status": strconv.FormatBool(vapp.vappClient.VApp.InMaintenanceMode),
			"org_id": data.id,
			"vdc_id": vapp.vdcId,
		})
		if vapp.vappClient.VApp.Status > 0 && vapp.vappClient.VApp.Status < 5 {
			gauge.Set(1)
		} else {
			gauge.Set(0)
		}
	}
	return nil
}

func (e *Exporter) setVmMetrics(data *vOrg) error {
	for _, vapp := range data.vapps {
		for _, vm := range vapp.vms {
			// TODO: add more metrics here
			var networks []map[string]string
			for _, network := range vm.networkConnectionSection.NetworkConnection {
				netMap := make(map[string]string)
				netMap["network_name"] = network.Network
				netMap["network_state"] = strconv.FormatBool(network.IsConnected)
				networks = append(networks, netMap)
			}
			networkLabels := flattenMaps(networks)

			gauge := e.gaugeVecs["vmStatus"].With(prometheus.Labels{
				"vm_name":                 vm.vmClient.VM.Name,
				"vm_id":                   vm.vmClient.VM.ID,
				"vm_status":               strconv.Itoa(vm.vmClient.VM.Status),
				"vm_storage_profile_name": vm.vmClient.VM.StorageProfile.Name,
				"vm_network_name_state":   networkLabels,
				"vapp_id":                 vm.vappId,
				"org_id":                  data.id,
				"vdc_id":                  vm.vdcId,
			})

			if vm.vmClient.VM.Status == 1 {
				gauge.Set(1)
			} else {
				gauge.Set(0)
			}
		}
	}
	return nil
}

func (e *Exporter) setVdcNetworkMetrics(data *vOrg) error {
	for _, network := range data.vdcnetworks.vdcnetwork {
		if network.OrgVDCNetwork.EdgeGateway == nil {
			gauge := e.gaugeVecs["vdcNetworkStatus"].With(prometheus.Labels{
				"network_name":                          network.OrgVDCNetwork.Name,
				"network_id":                            network.OrgVDCNetwork.ID,
				"network_status":                        network.OrgVDCNetwork.Status,
				"network_edge_gateway_id":               "0",
				"network_parent_network_id":             "0",
				"network_parent_network_name":           "0",
				"network_feature_static_routing_status": "0",
				"network_feature_nat_status":            "0",
				"network_feature_load_balancer_status":  "0",
				"network_feature_firewall_status":       "0",
				"network_feature_dhcp_status":           "0",
				"vdc_id":                                data.vdcnetworks.vdcId,
				"org_id":                                data.id,
			})

			//log.Info(network.OrgVDCNetwork.Status)
			if network.OrgVDCNetwork.Status == "1" {
				gauge.Set(1)
			} else {
				gauge.Set(0)
			}
		} else {
			gauge := e.gaugeVecs["vdcNetworkStatus"].With(prometheus.Labels{
				"network_name":                          network.OrgVDCNetwork.Name,
				"network_id":                            network.OrgVDCNetwork.ID,
				"network_status":                        network.OrgVDCNetwork.Status,
				"network_edge_gateway_id":               network.OrgVDCNetwork.EdgeGateway.ID,
				"network_parent_network_id":             network.OrgVDCNetwork.Configuration.ParentNetwork.ID,
				"network_parent_network_name":           network.OrgVDCNetwork.Configuration.ParentNetwork.Name,
				"network_feature_static_routing_status": strconv.FormatBool(network.OrgVDCNetwork.Configuration.Features.StaticRoutingService.IsEnabled),
				"network_feature_nat_status":            strconv.FormatBool(network.OrgVDCNetwork.Configuration.Features.NatService.IsEnabled),
				"network_feature_load_balancer_status":  strconv.FormatBool(network.OrgVDCNetwork.Configuration.Features.LoadBalancerService.IsEnabled),
				"network_feature_firewall_status":       strconv.FormatBool(network.OrgVDCNetwork.Configuration.Features.FirewallService.IsEnabled),
				"network_feature_dhcp_status":           strconv.FormatBool(network.OrgVDCNetwork.Configuration.Features.DhcpService.IsEnabled),
				"vdc_id":                                data.vdcnetworks.vdcId,
				"org_id":                                data.id,
			})

			//log.Info(network.OrgVDCNetwork.Status)
			if network.OrgVDCNetwork.Status == "1" {
				gauge.Set(1)
			} else {
				gauge.Set(0)
			}
		}
	}
	return nil
}

func (e *Exporter) setVdcStorageProfileMetrics(data *vOrg) error {
	for _, storageProfile := range data.vdcstorageprofiles {
		gauge := e.gaugeVecs["vdcStorageProfileStatus"].With(prometheus.Labels{
			"storage_profile_name": storageProfile.name,
			"storage_profile_id": storageProfile.id,
			"storage_profile_provisioned_mb": strconv.FormatInt(storageProfile.provisionedMB, 10),
			"storage_profile_requested_mb": strconv.FormatInt(storageProfile.requestedMB, 10),
			"storage_profile_total_mb": strconv.FormatInt(storageProfile.totalMB, 10),
			"storage_profile_used_mb": strconv.FormatInt(storageProfile.usedMB, 10),
			"vdc_id": storageProfile.vdcId,
			"org_id": data.id,
		})

		if storageProfile.usedMB != storageProfile.totalMB {
			gauge.Set(1)
		} else {
			gauge.Set(0)
		}
	}
	return nil
}

func (e *Exporter) setEdgeGatewayMetrics(data *vOrg) error {
	for _, edgeGateway := range data.edgegateways.edgeGateways {
		gauge := e.gaugeVecs["edgeGatewayStatus"].With(prometheus.Labels{
			"edge_gateway_name": edgeGateway.EdgeGateway.Name,
			"edge_gateway_id": edgeGateway.EdgeGateway.ID,
			"edge_gateway_status": strconv.Itoa(edgeGateway.EdgeGateway.Status),
			"edge_gateway_tasks": strconv.Itoa(len(edgeGateway.EdgeGateway.Tasks.Task)),
			"edge_gateway_firewall_service_status": strconv.FormatBool(edgeGateway.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.IsEnabled),
			"edge_gateway_dhcp_service_status": strconv.FormatBool(edgeGateway.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.GatewayDhcpService.IsEnabled),
			"edge_gateway_ipsec_service_status": strconv.FormatBool(edgeGateway.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.GatewayIpsecVpnService.IsEnabled),
			"edge_gateway_load_balancer_service_status": strconv.FormatBool(edgeGateway.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.LoadBalancerService.IsEnabled),
			"edge_gateway_nat_service_status": strconv.FormatBool(edgeGateway.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.NatService.IsEnabled),
			"edge_gateway_static_route_service_status": strconv.FormatBool(edgeGateway.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.StaticRoutingService.IsEnabled),
			"vdc_id": data.edgegateways.vdcId,
			"org_id": data.id,
		})

		log.Info(edgeGateway.EdgeGateway.Status)
		if edgeGateway.EdgeGateway.Status == 1 {
			gauge.Set(1)
		} else {
			gauge.Set(0)
		}
	}

	return nil
}