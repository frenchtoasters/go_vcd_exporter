FROM golang:1.9.1-alpine3.6 as builder
LABEL maintainer="Tyler French"

COPY . /go/src/github.com/vcd-exporter

RUN apk --update add ca-certificates \
 && apk --update add --virtual build-deps go git make

WORKDIR /go/src/github.com/vcd-exporter

RUN cd vendor/github.com/vmware/go-vcloud-director/v2/govcd \
 && GOPATH=/go go get \
 && GOPATH=/go go build

RUN GOPATH=/go go get \
 && GOPATH=/go go build vcd_exporter.go log.go prometheus.go exporter.go metrics.go gather.go \
 && mv vcd_exporter /bin/ \
 && apk del --purge build-deps \
 && rm -rf /go/bin /go/pkg /var/cache/apk/*

FROM alpine:latest

EXPOSE 9173

RUN addgroup exporter \
 && adduser -S -G exporter exporter \
 && apk --no-cache add ca-certificates

COPY --from=builder /bin/vcd_exporter /bin/vcd_exporter

RUN mkdir /opt/vcd_config \
 && chown exporter /opt/vcd_config \
 && chown exporter /bin/vcd_exporter \
 && chmod u+x /bin/vcd_exporter

USER exporter

ENTRYPOINT [ "/bin/vcd_exporter" ]