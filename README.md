# vCloud Director Prometheus exporter

## How to build and run

```bash
$> git clone https://gitlab.com/frenchtoasters/go_vcd_exporter
$> cd go_vcd_exporter
$> mkdir vcd_config
$> touch vcd_config/vcd_config.yml
$> docker build -t go_vcd_exporter .
$> docker run -d -p 9173:9173 -v $PWD/vcd_config:/opt/vcd_config go_vcd_exporter 
```

## Configuration 

You should only provide a configuration file if the environment variables are not going to be set. If you are going to use a configuration file use a volume like so:

```bash
$> docker run -it --rm -e LOG_LEVEL="info" \
 -e METRICS_PATH="/metrics" \
 -e LISTEN_ADDRESS=":9173" \
 -e CONFIG_PATH="/opt/vcd_config/vcd_config.yml" \
 -p 9173:9173 \
 -v $PWD/vcd_config:/path/to/config \
 --name vcd_exporter registry.gitlab.com/frenchtoasters/go_vcd_exporter:latest
```


The following is what an example `vcd_config.yml` file would look like:

```yaml
vcd_endpoints:
  - vcd_host: "vcdtest.duckdns.org"
    vcd_org: "system"
    vcd_user: "administrator"
    vcd_password: 'password'
    ignore_ssl: False
    vm_data: False
    net_data: True
    storage_data: True
  - vcd_host: "vcdtest.duckdns.org1"
    vcd_org: "system"
    vcd_user: "administrator"
    vcd_password: 'password'
    ignore_ssl: True
    vm_data: True
    net_data: False
    storage_data: True
```

### Environment/Configuration  Variables

| Variable       | Precedence             | Defaults | Description                                       |
|----------------|------------------------|----------|---------------------------------------------------|
| METRICS_PATH   | env                    | /metrics      | Promtheus metrics path                       |
| LISTEN_ADDRESS | env                    | :9173      | Port for promtheus metrics                        |
| CONFIG_PATH    | env                    | /opt/vcd_config/vcd_config.yml | Path for vCD configuration file    |
| vcd_host | config | n/a | URL of vCD api |
| vcd_org | config | n/a | vOrg Name of vcd_user |
| vcd_user | config | n/a | vCD Username |
| vcd_password | config | n/a | vCD Password |
| ignore_ssl | config | n/a | Flag to ignore self signed cert |
| vm_data | config | n/a | Flag to trigger VM data collection |
| net_data | config | n/a | Flag to trigger vDC Network data collection |
| storage_data | config | n/a | Flag to trigger Storage data collection | 


### Prometheus configuration

The following the base Prometheus configuration file.

```yaml
  # Default config
  - job_name: 'vcd_exporter'
    metrics_path: '/metrics'
    static_configs:
    - targets: ['localhost:9173']
      labels: 
        group: 'vcd-localhost'
  
  # Metrics Config
  - job_name: 'vcd_exporter_metrics'
    static_configs:
    - targets: ['localhost:9173']
      labels: 
        group: 'vcd-gather-metrics'
```

# References

The vCloud Director exporter uses the following libraries for data collection:

* [go-vcloud-director](https://github.com/vmware/go-vcloud-director) for vCD connection
* Prometheus [client_golang](https://github.com/prometheus/client_golang) for Prometheus metrics

Original code inspired by: 
* Rancher Exporter [promtheus-rancher-exporter](https://github.com/infinityworks/prometheus-rancher-exporter) 