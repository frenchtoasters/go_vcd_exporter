package main

import "github.com/prometheus/client_golang/prometheus"

// Rest the guageVecs to 0
func (e *Exporter) resetGaugeVecs() {
	for _, m := range e.gaugeVecs {
		m.Reset()
	}
}

// Describe the metrics that were exported
func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	for _, m := range e.gaugeVecs {
		m.Describe(ch)
	}
}

// Collect function to collect Prometheus metrics
func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.mutex.Lock() // protection for concurrent calls of Collect
	defer e.mutex.Unlock()

	e.resetGaugeVecs() // Clean to start

	// Gather the metrics
	var data, err = e.gatherMetrics(e)

	if err != nil {
		log.Errorf("Unable to Gather Metrics: %s \n", err)
	}
	//log.Printf("Data: %s\n", data)
	if err := e.processMetrics(data, ch, e); err != nil {
		log.Errorf("Unable to Process Metrics: %s \n", err)
	}
	log.Infof("Metrics successfully processed %s vOrgs", len(data.Orgs))


	for _, m := range e.gaugeVecs {
		m.Collect(ch)
	}
}
