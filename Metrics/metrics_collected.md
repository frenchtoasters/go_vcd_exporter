# Metrics Currently Collected

The following is a list of the metrics and their labels that are currently being gathered:

* Go Program Stats

* promhttp stats

* vcd_org_health_status 

```
vcd_org_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",org_name="Lobster-Shack",org_status="false"} 0
vcd_org_health_status{org_id="urn:vcloud:org:68b0250e-bddf-4e1c-8cd2-0861e15bb8a1",org_name="backup",org_status="false"} 0
vcd_org_health_status{org_id="urn:vcloud:org:84ceec06-e76d-4d45-83fe-ba9ceeec0497",org_name="vcdtest",org_status="false"} 0
```

* vcd_vapp_health_status

```
vcd_vapp_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vapp_id="urn:vcloud:vapp:276e345a-3b35-436f-85cb-42242c0421d6",vapp_maintenance_status="false",vapp_name="Web",vapp_status="POWERED_OFF",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c"} 0
vcd_vapp_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vapp_id="urn:vcloud:vapp:ba2291d4-d730-4ea7-8bfe-7edadfa7bb94",vapp_maintenance_status="false",vapp_name="App",vapp_status="POWERED_OFF",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c"} 0
vcd_vapp_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vapp_id="urn:vcloud:vapp:f4a71494-8d93-4588-80d4-fef3f9fdedc5",vapp_maintenance_status="false",vapp_name="Cloud Connect 1",vapp_status="RESOLVED",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c"} 1
```

* vcd_vdc_health_status

```
vcd_vdc_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vdc_allocation_model="AllocationVApp",vdc_cpu_allocated="0",vdc_cpu_limit="0",vdc_cpu_overhead="0",vdc_cpu_reserved="0",vdc_cpu_units="MHz",vdc_cpu_used="",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c",vdc_is_enabled="true",vdc_mem_allocated="0",vdc_mem_limit="0",vdc_mem_overhead="0",vdc_mem_reserved="0",vdc_mem_units="MB",vdc_mem_used="0",vdc_name="DR",vdc_network_quota="1000",vdc_nic_quota="0",vdc_status="READY",vdc_used_network_count="2"} 1
vcd_vdc_health_status{org_id="urn:vcloud:org:68b0250e-bddf-4e1c-8cd2-0861e15bb8a1",vdc_allocation_model="AllocationVApp",vdc_cpu_allocated="0",vdc_cpu_limit="0",vdc_cpu_overhead="0",vdc_cpu_reserved="0",vdc_cpu_units="MHz",vdc_cpu_used="",vdc_id="urn:vcloud:vdc:0793231f-48d0-4e39-bef2-efbeed9027ad",vdc_is_enabled="true",vdc_mem_allocated="0",vdc_mem_limit="0",vdc_mem_overhead="0",vdc_mem_reserved="0",vdc_mem_units="MB",vdc_mem_used="0",vdc_name="bucket",vdc_network_quota="1000",vdc_nic_quota="0",vdc_status="READY",vdc_used_network_count="0"} 1
vcd_vdc_health_status{org_id="urn:vcloud:org:68b0250e-bddf-4e1c-8cd2-0861e15bb8a1",vdc_allocation_model="AllocationVApp",vdc_cpu_allocated="0",vdc_cpu_limit="0",vdc_cpu_overhead="0",vdc_cpu_reserved="0",vdc_cpu_units="MHz",vdc_cpu_used="",vdc_id="urn:vcloud:vdc:0a5900ad-f9b8-410f-9be2-f9029e57785a",vdc_is_enabled="true",vdc_mem_allocated="0",vdc_mem_limit="0",vdc_mem_overhead="0",vdc_mem_reserved="0",vdc_mem_units="MB",vdc_mem_used="0",vdc_name="backup",vdc_network_quota="1000",vdc_nic_quota="0",vdc_status="READY",vdc_used_network_count="0"} 1
vcd_vdc_health_status{org_id="urn:vcloud:org:84ceec06-e76d-4d45-83fe-ba9ceeec0497",vdc_allocation_model="AllocationVApp",vdc_cpu_allocated="0",vdc_cpu_limit="0",vdc_cpu_overhead="0",vdc_cpu_reserved="0",vdc_cpu_units="MHz",vdc_cpu_used="",vdc_id="urn:vcloud:vdc:d229df09-ba75-48c8-bbd3-bb29946f5c1e",vdc_is_enabled="true",vdc_mem_allocated="0",vdc_mem_limit="0",vdc_mem_overhead="0",vdc_mem_reserved="0",vdc_mem_units="MB",vdc_mem_used="0",vdc_name="vcdtest",vdc_network_quota="1000",vdc_nic_quota="0",vdc_status="READY",vdc_used_network_count="0"} 1
```

* vcd_vm_health_status

```
vcd_vm_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vapp_id="urn:vcloud:vapp:ba2291d4-d730-4ea7-8bfe-7edadfa7bb94",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c",vm_id="urn:vcloud:vm:393da289-6f35-4d19-82cd-67ba4793ce6e",vm_name="app2.lobstershack.com_replica",vm_network_name="so close",vm_network_state="so close",vm_status="8",vm_storage_profile_name="vcd-storage"} 0
vcd_vm_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vapp_id="urn:vcloud:vapp:ba2291d4-d730-4ea7-8bfe-7edadfa7bb94",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c",vm_id="urn:vcloud:vm:7ae8d5dc-d465-44fd-a60c-0540f28aba45",vm_name="app1.lobstershack.com_replica",vm_network_name="so close",vm_network_state="so close",vm_status="8",vm_storage_profile_name="vcd-storage"} 0
vcd_vm_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vapp_id="urn:vcloud:vapp:ba2291d4-d730-4ea7-8bfe-7edadfa7bb94",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c",vm_id="urn:vcloud:vm:c1876560-db84-4f2c-bd4f-302e468ae4b4",vm_name="web2.lobstershack.com_replica",vm_network_name="so close",vm_network_state="so close",vm_status="8",vm_storage_profile_name="vcd-storage"} 0
vcd_vm_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vapp_id="urn:vcloud:vapp:ba2291d4-d730-4ea7-8bfe-7edadfa7bb94",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c",vm_id="urn:vcloud:vm:d7848f3a-c848-4601-b81e-f99e2e427d9e",vm_name="web1.lobstershack.com_replica",vm_network_name="so close",vm_network_state="so close",vm_status="8",vm_storage_profile_name="vcd-storage"} 0
```

* vdc_network_health_status

```
vcd_vdc_network_health_status{network_edge_gateway_id="0",network_feature_dhcp_status="0",network_feature_firewall_status="0",network_feature_load_balancer_status="0",network_feature_nat_status="0",network_feature_static_routing_status="0",network_id="urn:vcloud:network:d00e637f-2463-475e-b4e8-dae65e94eae7",network_name="App",network_parent_network_id="0",network_parent_network_name="0",network_status="1",org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c"} 1
```

* vdc_storage_profile_health_status

```
vcd_vdc_storage_profile_health_status{org_id="urn:vcloud:org:0be49a83-0e85-460a-b1b4-9ac84a4de480",storage_profile_id="urn:vcloud:vdcstorageProfile:c4df0386-d221-420c-8a8d-574b353c1283",storage_profile_name="vcd-storage",storage_profile_provisioned_mb="3707423",storage_profile_requested_mb="262144",storage_profile_total_mb="5624109",storage_profile_used_mb="3473281",vdc_id="urn:vcloud:vdc:12aa5168-bd0b-4958-acf8-2b40f706a81c"} 1
vcd_vdc_storage_profile_health_status{org_id="urn:vcloud:org:68b0250e-bddf-4e1c-8cd2-0861e15bb8a1",storage_profile_id="urn:vcloud:vdcstorageProfile:4a98f6fe-2995-45ec-b9ce-d53ccf66ef1e",storage_profile_name="vcd-storage",storage_profile_provisioned_mb="3707423",storage_profile_requested_mb="262144",storage_profile_total_mb="5624109",storage_profile_used_mb="3473281",vdc_id="urn:vcloud:vdc:0793231f-48d0-4e39-bef2-efbeed9027ad"} 1
vcd_vdc_storage_profile_health_status{org_id="urn:vcloud:org:68b0250e-bddf-4e1c-8cd2-0861e15bb8a1",storage_profile_id="urn:vcloud:vdcstorageProfile:8152ae54-4b5c-4bf3-9690-7be26df7e836",storage_profile_name="vcd-storage",storage_profile_provisioned_mb="3707423",storage_profile_requested_mb="262144",storage_profile_total_mb="5624109",storage_profile_used_mb="3473281",vdc_id="urn:vcloud:vdc:0a5900ad-f9b8-410f-9be2-f9029e57785a"} 1
vcd_vdc_storage_profile_health_status{org_id="urn:vcloud:org:84ceec06-e76d-4d45-83fe-ba9ceeec0497",storage_profile_id="urn:vcloud:vdcstorageProfile:96c7c002-b9f7-4a2c-9138-a2a78023a5b3",storage_profile_name="vcd-storage",storage_profile_provisioned_mb="3707423",storage_profile_requested_mb="262144",storage_profile_total_mb="5624109",storage_profile_used_mb="3473281",vdc_id="urn:vcloud:vdc:d229df09-ba75-48c8-bbd3-bb29946f5c1e"} 1
```

* vdc_edge_gateway_health_status

```
Missing example
```